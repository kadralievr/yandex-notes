//
//  EditNoteViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 09/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

protocol NoteEditDelegate: NSObjectProtocol {
    func edited(note: Note)
}

// Форма (сцена) редактирования заметки
class NoteEditViewController: UIViewController, ColorPickedDelegate {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextView!
    @IBOutlet var colorPickItems: [ColorPickItem]!
    @IBOutlet weak var whiteColorPickItem: ColorPickItem!
    @IBOutlet weak var redColorPickItem: ColorPickItem!
    @IBOutlet weak var greenColorPickItem: ColorPickItem!
    @IBOutlet weak var customColorPickItem: ColorPickItem!
    @IBOutlet weak var colorPalette: ColorPalette!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var useDestroyDateSwitch: UISwitch!
    @IBOutlet weak var datePickerHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var note: Note?
    weak var delegate: NoteEditDelegate?
    
    private var selectedColorItem: ColorPickItem?
    private var datePickerHeight: CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (note == nil) {
            note = Note(title: "", content: "", importance: .normal)
        }
        
        if let note = note {
            titleTextField.text = note.title
            contentTextField.text = note.content
            
            if let selfDestructionDate = note.selfDestructionDate {
                useDestroyDateSwitch.isOn = true
                datePicker.date = selfDestructionDate
            } else {
                useDestroyDateSwitch.isOn = false
            }
            
            switch (note.color) {
            case .white:
                selectedColorItem = whiteColorPickItem
            case .red:
                selectedColorItem = redColorPickItem
            case .green:
                selectedColorItem = greenColorPickItem
            default:
                customColorPickItem.color = note.color
                customColorPickItem.isHidden = false
                colorPalette.isHidden = true
                selectedColorItem = customColorPickItem
            }
        }
        
        changeDatePickerVisibility()
        
        selectedColorItem?.selected = true
        
        titleTextField.becomeFirstResponder()
        
        // Подписка на события изменения отображения клавиатуры
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Передача текущего кастомного цвета и делегата выбора нового при навигации на форму выбора цвета
        if let destination = segue.destination as? ColorPickerViewController {
            destination.color = customColorPickItem.color ?? .white
            destination.delegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent {
            
            let uid = note == nil ? UUID().uuidString : note!.uid
            let title = titleTextField.text ?? ""
            let content = contentTextField.text ?? ""
            let importance = Note.Importance.normal
            let selfDestructionDate = useDestroyDateSwitch.isOn ? datePicker.date : nil
            let color = selectedColorItem?.color ?? .white
            
            let note = Note(uid: uid, title: title, content: content, color: color, importance: importance, selfDestructionDate: selfDestructionDate)
            
            delegate?.edited(note: note)
        }
    }
    
    @IBAction func useDestroyDateSwitchChanged(_ sender: UISwitch) {
        changeDatePickerVisibility()
    }
    
    // Обработка события нажатия на элемент списка цветов. Снимает флаг выбора с текущего, устанавливает флаг выбора на новом
    @IBAction func colorPickItemTapped(_ sender: UITapGestureRecognizer) {
        let tappedColorItem = sender.view as? ColorPickItem
        selectedColorItem?.selected = false
        selectedColorItem = tappedColorItem
        selectedColorItem?.selected = true
    }
    
    // Переход на форму выбора цвета при долгом нажатии на палитре или на элементе кастомного цвета
    @IBAction func colorPaletteLongPressed(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
            performSegue(withIdentifier: "ColorPickerSegue", sender: self)
        }
    }
    
    // Обработка выбора цвета с помощью палитры. Скрывает палитру из списка, отображает новый элемент с выбранным цветом и установленным флагом выбора
    func colorPicked(color: UIColor) {
        customColorPickItem.color = color
        customColorPickItem.isHidden = false
        
        colorPalette.isHidden = true
        
        selectedColorItem?.selected = false
        selectedColorItem = customColorPickItem
        selectedColorItem?.selected = true
    }
    
    // Изменение размеров видимой части scrollView при появлении/скрытии клавиатуры
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    // Изменение видимости выбора даты
    private func changeDatePickerVisibility() {
        if (useDestroyDateSwitch.isOn) {
            datePicker.isHidden = false
            if let datePickerHeight = datePickerHeight {
                datePickerHeightContraint.constant = datePickerHeight
            }
        } else {
            datePickerHeight = datePickerHeightContraint.constant
            datePickerHeightContraint.constant = 0
            datePicker.isHidden = true
        }
    }

}
