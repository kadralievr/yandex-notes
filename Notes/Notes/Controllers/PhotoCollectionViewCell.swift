//
//  PhotoCollectionViewCell.swift
//  Notes
//
//  Created by Kadraliev Ramil on 21/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        frameView.layer.borderColor = UIColor.lightGray.cgColor
        frameView.layer.borderWidth = 1.0
        frameView.layer.cornerRadius = 4
    }

}
