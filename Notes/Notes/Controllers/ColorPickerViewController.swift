//
//  ColorPickerViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 11/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

// Форма (сцена) выбора цвета с помощью цветовой палитры. Располагает представление ColorPicker с ручным расположением элементов управления
class ColorPickerViewController: UIViewController, ColorTouchedDelegate, ColorPickedDelegate {
    
    var color: UIColor?

    // Представление выбора цвета
    @IBOutlet weak var colorPicker: ColorPicker!
    
    weak internal var delegate: ColorPickedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Подписка на события изменения и выбора цвета
        colorPicker.colorTouchedDelegate = self
        colorPicker.colorPickedDelegate = self
    }
    
    // Переход на текущий цвет после расположения всех вложенных представлений
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let color = color {
            colorPicker.pick(color)
        }
    }
    
    // Переход на текущий цвет после отрисовки
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let color = color {
            colorPicker.pick(color)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Переход текущего цвета после изменения ориентации экрана
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            if let color = self.color {
                self.colorPicker.pick(color)
            }
        })
    }
    
    // Инициирует событие выбора цвета в цветовой палите и закрывает форму выбора цвета
    func colorPicked(color: UIColor) {
        delegate?.colorPicked(color: color)
        navigationController?.popViewController(animated: true)
    }
    
    // Сохранение цвета, чтобы перейти на него в случае изменения ориентации экрана
    func colorTouched(color: UIColor) {
        self.color = color
    }

}
