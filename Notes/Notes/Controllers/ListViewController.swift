//
//  ListViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 20/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack
import CoreData

class ListViewController: UITableViewController {
    
    @IBOutlet var notesTableView: UITableView!
    
    // Фоновый контекст данных для сохранения в фоновом потоке
    var backgroundContext: NSManagedObjectContext!
    
    private var accessToken: String?
    private var gistID: String?
    
    // Очередь выполнения DB операций
    private let dbOperationQueue = OperationQueue()
    
    // Очередь выполнения Backend операций
    private let backendOperationQueue = OperationQueue()
    
    // Очередь выполнения аггрегирующих операций
    private let backgroundOperationQueue = OperationQueue()
    
    private var notes = [Note]()
    private let noteCellReuseIdentifier = "noteCell"
    private var selectedNote: Note?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notesTableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: noteCellReuseIdentifier)
        
        // В каждой очереди операции выполняются последовательно
        dbOperationQueue.maxConcurrentOperationCount = 1
        backendOperationQueue.maxConcurrentOperationCount = 1
        backgroundOperationQueue.maxConcurrentOperationCount = 1
        
        if (gistID == nil) {
            loadGistID()
        }
        
        notesTableView.refreshControl = UIRefreshControl()
        notesTableView.refreshControl?.beginRefreshing()
        
        if (accessToken == nil) {
            performSegue(withIdentifier: "AuthSegue", sender: self)
        } else {
            loadNotes()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let editViewController = segue.destination as? NoteEditViewController {
            editViewController.note = selectedNote
            editViewController.delegate = self
        } else if let authViewController = segue.destination as? AuthViewController {
            authViewController.delegate = self
        }
    }
    
    @IBAction func editListButtonTapped(_ sender: UIBarButtonItem) {
        notesTableView.isEditing = !notesTableView.isEditing
        if notesTableView.isEditing {
            sender.title = "Готово"
        } else {
            sender.title = "Редактировать"
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        selectedNote = Note(title: "", content: "", importance: .normal)
        performSegue(withIdentifier: "EditSegue", sender: self)
    }
    
    private func loadNotes() {        
        if let accessToken = accessToken {
            // инициализация операции загрузки списка заметок
            let loadNotesOperation = LoadNotesOperation(backendQueue: backendOperationQueue, dbQueue: dbOperationQueue, accessToken: accessToken, gistID: gistID, context: backgroundContext)
            
            // по завершению операции загрузки
            loadNotesOperation.completionBlock = {
                
                // асинхронно в main очереди
                DispatchQueue.main.async {
                    if let loadNotesResult = loadNotesOperation.result {
                        switch loadNotesResult {
                            
                        case .success(let notes):
                            // обновляем модель и таблицу в UI
                            self.notes = notes
                            self.notesTableView.reloadData()
                            
                            if self.gistID == nil {
                                self.gistID = ""
                            }
                            
                        case .gistCreated(let gistID, let notes):
                            // обновляем модель и таблицу в UI
                            self.notes = notes
                            self.notesTableView.reloadData()
                            
                            self.gistID = gistID
                            self.saveGistID()
                            
                            let backendOperation = SaveNotesBackendOperation(accessToken: accessToken, gistID: gistID, notes: self.notes)
                            self.backendOperationQueue.addOperation(backendOperation)
                            
                        case .failure:
                            DDLogError("Notes loading finished with failure")
                        }
                        
                        self.notesTableView.refreshControl?.endRefreshing()
                    }
                }
            }
            
            // ставим операцию загрузки на очередь выполнения
            backgroundOperationQueue.addOperation(loadNotesOperation)
        }
    }
    
    public func saveGistID() {
        if let gistID = gistID,
            let data = gistID.data(using: String.Encoding.utf8),
            let cachesPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let directoryPath = cachesPath.appendingPathComponent("YandexCourse_NotesApp")
            var isDirectory: ObjCBool = false
            
            if (!FileManager.default.fileExists(atPath: directoryPath.path, isDirectory: &isDirectory) || !isDirectory.boolValue) {
                do {
                    try FileManager.default.createDirectory(at: directoryPath, withIntermediateDirectories: true, attributes: nil)
                } catch {
                }
            }
            
            let filePath = directoryPath.appendingPathComponent("GistID")
            
            do {
                try data.write(to: filePath)
                DDLogInfo("Saved GistID \(gistID) to file")
            } catch {
            }
        }
    }
    
    public func loadGistID() {
        if let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let directoryPath = documentsPath.appendingPathComponent("YandexCourse_NotesApp")
            let filePath = directoryPath.appendingPathComponent("GistID")
            
            // генерация ошибки, если файла не существует
            if FileManager.default.fileExists(atPath: filePath.path) {
                do {
                    let data = try Data(contentsOf: filePath)
                    if let gistID = String(data: data, encoding: String.Encoding.utf8) {
                        self.gistID = gistID
                        DDLogInfo("Found GistID: \(gistID)")
                    }
                } catch {
                }
            }
        }
    }
}

extension ListViewController: AuthDelegate {
    
    func tokenObtained(token: String) {
        DDLogInfo("Access token is obtained: \(token)")
        self.accessToken = token
        loadNotes()
    }
    
    func tokenObtainmentFailed() {
        self.accessToken = ""
        loadNotes()
    }
    
}

extension ListViewController: NoteEditDelegate {
    
    func edited(note: Note) {
        
        if let index = notes.firstIndex(where: {$0.uid == note.uid}) {
            notes[index] = note
            let indexPath = IndexPath(row: index, section: 0)
            self.notesTableView.reloadRows(at: [indexPath], with: .automatic)
        } else {
            notes.append(note)
            let indexPath = IndexPath(row: notes.count - 1, section: 0)
            self.notesTableView.insertRows(at: [indexPath], with: .automatic)
        }
        
        // инициализация операции сохранения списка заметок
        let saveNoteOperation = SaveNoteOperation(note: note, notes: self.notes, context: backgroundContext, backendQueue: backendOperationQueue, dbQueue: dbOperationQueue, accessToken: self.accessToken!, gistID: gistID!)
        
        // по завершению операции сохранения
        saveNoteOperation.completionBlock = {
            
            // асинхронно в main очереди
            DispatchQueue.main.async {
                if let saveNoteResult = saveNoteOperation.result {
                    switch saveNoteResult {
                        
                    // в случае успеха
                    case .success:
                        DDLogError("Notes saving finished with failure")
                        
                    case .failure:
                        DDLogError("Notes saving finished with failure")
                    }
                }
            }
        }
        
        // ставим операцию сохранения на очередь выполнения
        backgroundOperationQueue.addOperation(saveNoteOperation)
    }
    
}

extension ListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notesTableView.dequeueReusableCell(withIdentifier: noteCellReuseIdentifier, for: indexPath) as! NoteTableViewCell
        let note = notes[indexPath.row]
        
        cell.colorView.backgroundColor = note.color
        cell.titleLabel.text = note.title
        cell.contentLabel.text = note.content
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let note = notes[indexPath.row]
            
            notes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            // инициализация операции удаления заметки
            let removeNoteOperation = RemoveNoteOperation(uid: note.uid, notes: notes, context: backgroundContext, backendQueue: backendOperationQueue, dbQueue: dbOperationQueue, accessToken: accessToken!, gistID: gistID!)
            
            // по завершению операции удаления
            removeNoteOperation.completionBlock = {
                
                // асинхронно в main очереди
                DispatchQueue.main.async {
                    if let removeNoteResult = removeNoteOperation.result {
                        switch removeNoteResult {
                            
                        // в случае успеха
                        case .success:
                            // обновляем таблицу в UI
                            DDLogError("Note remove succeeded")
                            
                        case .failure:
                            DDLogError("Note remove finished with failure")
                        }
                    }
                }
            }
            
            backgroundOperationQueue.addOperation(removeNoteOperation)
        } else {
            super.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedNote = notes[indexPath.row]
        performSegue(withIdentifier: "EditSegue", sender: self)
    }
    
}
