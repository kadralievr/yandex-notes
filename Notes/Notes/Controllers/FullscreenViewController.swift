//
//  FullscreenViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 21/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

protocol PhotoSelectedIndexChangedDelegate: NSObjectProtocol {
    func selectedIndexChanged(index: Int)
}

class FullscreenViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var photos: [UIImage]?
    var selectedIndex: Int?
    weak var delegate: PhotoSelectedIndexChangedDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let photos = photos, let selectedIndex = selectedIndex {
            let width = view.frame.width
            let height = view.frame.height
            
            for i in 0..<photos.count {
                
                let imageView = UIImageView()
                imageView.frame = CGRect(x: CGFloat(i) * width, y: 0.0, width: width, height: height)
                imageView.image = photos[i]
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                
                scrollView.contentSize.width = view.frame.size.width * CGFloat(i + 1)
                scrollView.addSubview(imageView)
            }
            
            pageControl.numberOfPages = photos.count
            pageControl.currentPage = selectedIndex
            
            setOffset()
        }
        
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        
//        // Переход текущего цвета после изменения ориентации экрана
//        coordinator.animate(alongsideTransition: nil, completion: { _ in
//            self.setOffset()
//        })
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.selectedIndexChanged(index: pageControl.currentPage)
    }
    
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        setOffset()
    }
    
    private func setOffset() {
        let offset = CGFloat(pageControl.currentPage) * scrollView.frame.width
        scrollView.setContentOffset(CGPoint(x: offset, y: 0), animated: true)
    }
    
}

extension FullscreenViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
        pageControl.currentPage = pageIndex
    }
}
