//
//  PhotoListViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 21/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

class PhotoListViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PhotoSelectedIndexChangedDelegate {

    @IBOutlet var photosCollectionView: UICollectionView!
    
    private let reuseIdentifier = "imageCell"
    private var photos: [UIImage] = []
    private let imagePickerController = UIImagePickerController()
    private var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photosCollectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        for i in 1...5 {
            if let photo = UIImage(named: String(i)) {
                photos.append(photo)
            }
        }
        
        imagePickerController.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let fullscreanViewController = segue.destination as? FullscreenViewController {
            fullscreanViewController.photos = photos
            fullscreanViewController.selectedIndex = selectedIndex
            fullscreanViewController.delegate = self
        }
    }
    
    @IBAction func addPhotoTapped(_ sender: UIBarButtonItem) {
        imagePickerController.allowsEditing = false
        imagePickerController.sourceType = .photoLibrary
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedPhoto = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            photos.append(pickedPhoto)
            let indexPath = IndexPath(row: photos.count - 1, section: 0)
            collectionView.insertItems(at: [indexPath])
            scroll(to: indexPath.row)
        }
        
        dismiss(animated: true, completion: nil)
    }    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func selectedIndexChanged(index: Int) {
        scroll(to: index)
    }
    
    private func scroll(to index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
    }
}

extension PhotoListViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
        
        cell.imageView.image = photos[indexPath.row]
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "FullscreenSegue", sender: self)
    }
    
}

extension PhotoListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3
        let result = CGSize(width: width, height: width)
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}
