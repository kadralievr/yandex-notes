//
//  AuthViewController.swift
//  Notes
//
//  Created by Kadraliev Ramil on 09/08/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import WebKit
import CocoaLumberjack
import Network

protocol AuthDelegate: NSObjectProtocol {
    func tokenObtained(token: String)
    func tokenObtainmentFailed()
}

class AuthViewController: UIViewController {
    
    private static let ClientID = "8a4fc3c23ac7463ab1e7"
    private static let ClientSecret = "fbfc54dcff99859f60fbdab4fa4092cbd9cb7338"
    private static let CodeScheme = "mygists"
    private static let Scope = "gist"
    private static let AllowSignup = "false"
    
    private let state = UUID().uuidString
    private var networkStatus: NWPath.Status?
    
    var delegate: AuthDelegate?
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        
        let networkMonitor = NWPathMonitor()
        networkMonitor.pathUpdateHandler = { path in
            self.networkStatus = path.status
        }
        networkMonitor.start(queue: DispatchQueue.main)
        
        networkStatus = networkMonitor.currentPath.status
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let networkStatus = networkStatus,
            networkStatus == .satisfied,
            let request = self.buildGetCodeUrlRequest() {
            webView.load(request)
            webView.navigationDelegate = self
        } else {
            DDLogInfo("Network status is not satisfied")
            delegate?.tokenObtainmentFailed()
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func buildGetCodeUrlRequest() -> URLRequest? {
        
        guard var urlComponents = URLComponents(string: "https://github.com/login/oauth/authorize") else {
            return nil
        }
        
        urlComponents.queryItems = [
            URLQueryItem(name: "client_id", value: (AuthViewController.ClientID)),
            URLQueryItem(name: "redirect_uri", value: "\(AuthViewController.CodeScheme)://host.com/callback"),
            URLQueryItem(name: "scope", value: AuthViewController.Scope),
            URLQueryItem(name: "state", value: self.state),
            URLQueryItem(name: "allow_signup", value: AuthViewController.AllowSignup)
        ]
        
        guard let url = urlComponents.url else {
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        return request
    }
    
    private func buildGetTokenUrlRequest(code: String) -> URLRequest? {
        
        guard var urlComponents = URLComponents(string: "https://github.com/login/oauth/access_token") else {
            return nil
        }
        
        urlComponents.queryItems = [
            URLQueryItem(name: "client_id", value: AuthViewController.ClientID),
            URLQueryItem(name: "client_secret", value: AuthViewController.ClientSecret),
            URLQueryItem(name: "redirect_uri", value: "\(AuthViewController.CodeScheme)://host.com/callback"),
            URLQueryItem(name: "code", value: code),
            URLQueryItem(name: "state", value: self.state)
        ]
        
        guard let url = urlComponents.url else {
            return nil
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        
        return request
    }
    
}

extension AuthViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        // извлечение url Web View
        if let url = navigationAction.request.url {
            DDLogInfo("WebView url: \(url.absoluteURL)")
            
            // если получен callback
            if (url.scheme == AuthViewController.CodeScheme) {
                // разбираем URL
                if let components = URLComponents(string: url.absoluteString) {
                    // извлекаем временный code
                    if let code = components.queryItems?.first(where: { $0.name == "code" })?.value {
                        // строим запрос на получение access token на основе полученного временного кода
                        if let request = self.buildGetTokenUrlRequest(code: code) {
                            let task = URLSession.shared.dataTask(with: request) {
                                (data: Data?, response: URLResponse?, error: Error?) in
                                
                                if let error = error {
                                    // если запрос не отправлен, логируем ошибку
                                    DDLogError("An error has occurred during sending access token request: \(error.localizedDescription)")
                                    // и инициируем событие получения токена и возвращаемся на предыдущуюю форму
                                    DispatchQueue.main.async {
                                        self.delegate?.tokenObtainmentFailed()
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                } else if let response = response as? HTTPURLResponse {
                                    
                                    switch response.statusCode {
                                        
                                    // если получен успешный код ответа
                                    case 200..<300:
                                        // и тело ответа извлечено успешно
                                        if let data = data, let body = String(data: data, encoding: String.Encoding.utf8) {
                                            // исвлекаем access token из ответа
                                            let accessToken = body.split(separator: "&")[0].split(separator: "=")[1]
                                            // инициируем событие получения токена и возвращаемся на предыдущуюю форму
                                            DispatchQueue.main.async {
                                                self.delegate?.tokenObtained(token: String(accessToken))
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                        }
                                        
                                    // иначе логируем код ошибки
                                    default:
                                        DDLogError("Access token request has failed with error status code: \(response.statusCode)")
                                        
                                        // и инициируем событие получения токена и возвращаемся на предыдущуюю форму
                                        DispatchQueue.main.async {
                                            self.delegate?.tokenObtainmentFailed()
                                            self.navigationController?.popViewController(animated: true)
                                        }                                    }
                                }
                            }
                            
                            // выполняем запрос на получение токена
                            task.resume()
                        }
                    }
                }
            }
        }
        
        // вызов обработчика перехода WebView по url
        do {
            decisionHandler(.allow)
        }
    }
}
