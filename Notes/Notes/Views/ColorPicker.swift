//
//  ColorPicker.swift
//  Notes
//
//  Created by Kadraliev Ramil on 12/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

internal protocol ColorTouchedDelegate : NSObjectProtocol {
    // Вызывается при изменении цвета пользователем
    func colorTouched(color: UIColor)
}

internal protocol ColorPickedDelegate : NSObjectProtocol {
    // Вызывается при выборе цвета пользователем
    func colorPicked(color: UIColor)
}

// Представление выбора цвета с помощью палитры с ручной разметкой элементов управления
@IBDesignable
class ColorPicker : UIView, TargetMovedDelegate {
    
    weak internal var colorPickedDelegate: ColorPickedDelegate?
    weak internal var colorTouchedDelegate: ColorTouchedDelegate?
    
    // Расстояние между элементами
    private let subviewsMargin: CGFloat = 8
    
    // Размер элемента превью выбранного цвета
    @IBInspectable var colorPreviewBoxSize: CGSize = CGSize.zero {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private let colorPickItem = ColorPickItem()
    private let brightnessSlider = UISlider()
    private let brightnessLabel = UILabel()
    private let doneButton = UIButton()
    private let colorPalette = ColorPalette()
    private let target = Target()
    
    override var intrinsicContentSize: CGSize {
        let result = CGSize(width: colorPalette.bounds.width, height: colorPickItem.bounds.height + colorPalette.bounds.height + doneButton.bounds.height + 2 * subviewsMargin)
        return result
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // Располагает элементы управления
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Элемент превью цвета
        let hexRectHeight = colorPreviewBoxSize.height / 5;
        let size = CGSize(width: colorPreviewBoxSize.width, height: colorPreviewBoxSize.height + hexRectHeight)
        colorPickItem.frame = CGRect(origin: CGPoint(x: bounds.minX, y: bounds.minY), size: size)
        colorPickItem.hexRectHeight = hexRectHeight
        colorPickItem.displayHex = true
        colorPickItem.cornerRadius = 10
        
        // Слайдер яркости
        brightnessSlider.frame = CGRect(origin: CGPoint(x: bounds.minX + colorPickItem.frame.width + subviewsMargin, y: bounds.minY + colorPickItem.frame.height - brightnessSlider.intrinsicContentSize.height), size: CGSize(width: bounds.width - colorPickItem.frame.width - subviewsMargin, height: brightnessSlider.intrinsicContentSize.height))
        
        // Надпись яркости
        brightnessLabel.frame = CGRect(origin: CGPoint(x: bounds.minX + colorPickItem.frame.width + subviewsMargin, y: bounds.minY + colorPickItem.frame.height - brightnessSlider.intrinsicContentSize.height - subviewsMargin - brightnessLabel.intrinsicContentSize.height), size: brightnessLabel.intrinsicContentSize)
        
        // Кнопка Done (выбора цвета)
        doneButton.frame = CGRect(origin: CGPoint(x: bounds.midX - doneButton.intrinsicContentSize.width / 2, y: bounds.maxY - doneButton.intrinsicContentSize.height), size: doneButton.intrinsicContentSize)
        
        // Цветовая палитра
        colorPalette.frame = CGRect(origin: CGPoint(x: bounds.minX, y: bounds.minY + colorPickItem.frame.height + subviewsMargin), size: CGSize(width: bounds.width, height: bounds.height - colorPickItem.frame.height - doneButton.frame.height - 2 * subviewsMargin))
        
        // Представление мишени тех же размеров, что и палитра
        target.frame = colorPalette.frame
    }
    
    // Перемещает палитру на конкретный цвет
    func pick(_ color: UIColor) {
        colorPickItem.color = color

        let point = colorPalette.getPoint(of: color)
        target.moveTo(point)
        
        let brightness = getBrightness(of: color)        
        brightnessSlider.setValue(Float(brightness), animated: true)
    }
    
    // Обработчик события делегата TargetMovedDelegate (изменение коордиант курсора выбора цвета)
    func targetMoved(point: CGPoint) {
        let color = colorPalette.getColor(at: point)
        
        colorPickItem.color = color
        
        let brightness = getBrightness(of: color)
        brightnessSlider.setValue(Float(brightness), animated: true)
        
        colorTouchedDelegate?.colorTouched(color: color)
    }
    
    // Обработчик изменения значения слайдера яркости
    @objc func brightnessChanged() {
        
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        colorPickItem.color?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil)
        
        let color = UIColor(hue: hue, saturation: saturation, brightness: CGFloat(brightnessSlider.value), alpha: 1.0)
        
        colorPickItem.color = color
        
        let point = colorPalette.getPoint(of: color)
        target.moveTo(point)
        
        colorTouchedDelegate?.colorTouched(color: color)
    }
    
    // Обработчик события нажатия на кнопку Done (выбора цвета)
    @objc func doneTapped() {
        colorPickedDelegate?.colorPicked(color: colorPickItem.color!)
    }
    
    private func initialize() {
        clipsToBounds = true
        
        // Инициализация subview
        initializeSubview(colorPickItem)
        initializeSubview(brightnessSlider)
        initializeSubview(brightnessLabel)
        initializeSubview(doneButton)
        initializeSubview(colorPalette)
        initializeSubview(target)
        
        // Установка свойств
        colorPickItem.color = .green
        brightnessLabel.text = "Brightness:"
        doneButton.setTitle("Done", for: .normal)
        doneButton.setTitleColor(doneButton.tintColor, for: .normal)
        brightnessSlider.minimumValue = 0
        brightnessSlider.maximumValue = 1
        
        // Подписка на события
        target.delegate = self
        brightnessSlider.addTarget(self, action: #selector(brightnessChanged), for: .valueChanged)
        doneButton.addTarget(self, action: #selector(doneTapped), for: .touchUpInside)
    }
    
    // Инициализирует subview
    private func initializeSubview(_ subview: UIView){
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = true
    }
    
    func getBrightness(of color: UIColor) -> CGFloat {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);
        
        return brightness
    }
    
}
