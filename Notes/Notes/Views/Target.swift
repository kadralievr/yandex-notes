//
//  Target.swift
//  Notes
//
//  Created by Kadraliev Ramil on 14/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

internal protocol TargetMovedDelegate : NSObjectProtocol {    
    // Вызывается, если координаты мишени изменены пользовательским вводом (нажатием на экран)
    func targetMoved(point: CGPoint)
}

// Представление курсора выбора цвета
@IBDesignable
class Target : UIView {
    
    weak internal var delegate: TargetMovedDelegate?
    
    private let diameter: CGFloat = 30.0
    private let lineLength: CGFloat = 16.0
    
    // Текущие координаты курсора
    private var currentPoint: CGPoint = CGPoint.zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // Отрисовывает курсор
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let targetRect = CGRect(x: currentPoint.x - diameter / 2, y: currentPoint.y - diameter / 2, width: diameter, height: diameter)
        
        let circlePath = UIBezierPath(ovalIn: targetRect)
        circlePath.stroke()
        
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: targetRect.minX, y: targetRect.midY))
        linePath.addLine(to: CGPoint(x: targetRect.minX - lineLength, y: targetRect.midY))
        linePath.move(to: CGPoint(x: targetRect.midX, y: targetRect.minY))
        linePath.addLine(to: CGPoint(x: targetRect.midX, y: targetRect.minY - lineLength))
        linePath.move(to: CGPoint(x: targetRect.maxX, y: targetRect.midY))
        linePath.addLine(to: CGPoint(x: targetRect.maxX + lineLength, y: targetRect.midY))
        linePath.move(to: CGPoint(x: targetRect.midX, y: targetRect.maxY))
        linePath.addLine(to: CGPoint(x: targetRect.midX, y: targetRect.maxY + lineLength))
        linePath.stroke()
    }
    
    // Перемещает курсора после пользовательского ввода или при событиях других View (например, изменение цвета)
    func moveTo(_ point: CGPoint) {
        currentPoint = point
        setNeedsDisplay()
    }
    
    @objc func handlerLongPress(sender: UILongPressGestureRecognizer) {
        let point = sender.location(ofTouch: 0, in: self)
        
        moveTo(point)
        
        // Если курсор за границами видимости View, не инициируем событие изменения его координат
        if point.x >= bounds.minX && point.x <= bounds.maxX && point.y >= bounds.minY && point.y <= bounds.maxY {
            delegate?.targetMoved(point: currentPoint)
        }
    }
    
    private func initialize() {
        clipsToBounds = true
        backgroundColor = .clear
        
        // Добавление LongPress распознавателя жеста и обработчика наступления его события
        let longTapGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handlerLongPress(sender:)))
        longTapGestureRecognizer.minimumPressDuration = 0
        longTapGestureRecognizer.allowableMovement = CGFloat.greatestFiniteMagnitude
        addGestureRecognizer(longTapGestureRecognizer)
    }
    
}
