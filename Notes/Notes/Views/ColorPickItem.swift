//
//  ColorPickItem.swift
//  Notes
//
//  Created by Kadraliev Ramil on 11/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit

// Представление элемента списка быстрого выбора цвета, превью цвета
@IBDesignable
class ColorPickItem : UIView {
    
    // Цвет
    @IBInspectable var color: UIColor? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Маркер выбора
    @IBInspectable var selected: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Радиус углов
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Маркер отображения HEX-строки цвета
    @IBInspectable var displayHex: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Высота рамки, в которой отображается HEX-строка
    @IBInspectable var hexRectHeight: CGFloat = 0.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // Отрисовывет представление
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Отрисовка рамки и заливки
        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
        self.color?.setFill()
        path.fill()
        path.stroke()

        // Отрисовка флажка выбора, если установлен флаг selected
        if selected {
            let ovalMargin = CGFloat(10)
            let ovalWidth = CGFloat(30)

            let circleRect = CGRect(x: rect.maxX - ovalWidth - ovalMargin, y: rect.minY + ovalMargin, width: ovalWidth, height: ovalWidth)
            let circlePath = UIBezierPath(ovalIn: circleRect)
            circlePath.stroke()

            let checkPath = UIBezierPath()
            checkPath.move(to: CGPoint(x: rect.maxY - (3/4) * ovalWidth - ovalMargin, y: rect.minY + (1/4) * ovalWidth + ovalMargin))
            checkPath.addLine(to: CGPoint(x: rect.maxX - (1/2) * ovalWidth - ovalMargin, y: rect.minY + (3/4) * ovalWidth + ovalMargin))
            checkPath.addLine(to: CGPoint(x: rect.maxY - (1/4) * ovalWidth - ovalMargin, y: rect.minY + (1/4) * ovalWidth + ovalMargin))
            checkPath.lineWidth = 2
            checkPath.stroke()
        }

        // Отрисовка HEX-строки, если установлен флаг displayHex и цвет не пуст
        if displayHex && color != nil {
            let hexRectHeight: CGFloat = rect.height / 5
            let hexRect = CGRect(x: rect.minX, y: rect.maxY - hexRectHeight, width: rect.width, height: hexRectHeight)

            let hexPath = UIBezierPath(roundedRect: hexRect, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
            UIColor.white.setFill()
            hexPath.fill()
            hexPath.stroke()

            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0

            color?.getRed(&r, green: &g, blue: &b, alpha: &a)

            let hex = String(format: "#%02lX%02lX%02lX", lroundf(Float(r) * 255), lroundf(Float(g) * 255), lroundf(Float(b) * 255))

            let hexLabel = UILabel()
            hexLabel.text = hex
            hexLabel.textAlignment = .center
            hexLabel.font = hexLabel.font.withSize(rect.height / 7)
            hexLabel.drawText(in: hexRect)
        }
    }
    
    private func initialize() {
        backgroundColor = .clear
        contentMode = .redraw
    }
    
}
