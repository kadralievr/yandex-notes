//
//  ColorPalette.swift
//  Notes
//
//  Created by Kadraliev Ramil on 12/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit
import CocoaLumberjack

// Представление палитры
@IBDesignable
class ColorPalette : UIView {
    
    let saturationExponent: Float = 1.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // Отрисовывает палитру
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Отрисовка рамки
        let path = UIBezierPath(rect: rect)
        path.stroke()
        
        // Отрисовка палитры попиксельно
        let context = UIGraphicsGetCurrentContext()
        for y: CGFloat in stride(from: 0.0, to: rect.height, by: 1.0) {
            var saturation = y < rect.height / 2.0 ? (2 * y) / rect.height : 2.0 * (rect.height - y) / rect.height
            saturation = CGFloat(powf(Float(saturation), saturationExponent))
            let brightness = y < rect.height / 2.0 ? 1.0 : 2.0 * (rect.height - y) / rect.height
            for x: CGFloat in stride(from: 0.0, to: rect.width, by: 1.0) {
                let hue = x / rect.width
                let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
                context!.setFillColor(color.cgColor)
                context!.fill(CGRect(x: x, y: y, width: 1.0,height: 1.0))
            }
        }
    }
    
    // Возвращает цвет по координате
    func getColor(at point: CGPoint) -> UIColor {
        var saturation = point.y < self.frame.height / 2.0 ? (2 * point.y) / self.frame.height : 2.0 * (self.frame.height - point.y) / self.frame.height
        saturation = CGFloat(powf(Float(saturation), saturationExponent))
        
        let brightness = point.y < self.frame.height / 2.0 ? 1.0 : 2.0 * (self.frame.height - point.y) / self.frame.height
        
        let hue = point.x / self.frame.width
        
        let result = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        
        return result
    }
    
    // Возвращает координату по цвету
    func getPoint(of color: UIColor) -> CGPoint {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);
        
        var y: CGFloat = 0
        let halfHeight = self.frame.height / 2
        if brightness >= 0.99 {
            let percentageY = powf(Float(saturation), 1.0 / saturationExponent)
            y = CGFloat(percentageY) * halfHeight
        } else {
            y = halfHeight + halfHeight * (1.0 - brightness)
        }
        
        let x = hue * self.frame.width
        
        let result = CGPoint(x: x, y: y)
        
        return result
    }
    
    private func initialize() {
        self.clipsToBounds = true
    }
    
}
