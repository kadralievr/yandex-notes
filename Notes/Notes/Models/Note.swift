//
//  Note.swift
//  Notes
//
//  Created by Kadraliev Ramil on 28/06/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit

struct Note {
    
    enum Importance : Int {
        case unimportant = 0 // неважная
        case normal // обычная
        case important // важная
    }
    
    let uid: String
    let title: String
    let content: String
    let color: UIColor
    let importance: Importance
    let selfDestructionDate: Date?
    
    init(
        uid: String = UUID().uuidString,
        title: String,
        content: String,
        color: UIColor = UIColor.white,
        importance: Importance,
        selfDestructionDate: Date? = nil
    ) {
        self.uid = uid
        self.title = title
        self.content = content
        self.color = color
        self.importance = importance
        self.selfDestructionDate = selfDestructionDate
    }
    
}
