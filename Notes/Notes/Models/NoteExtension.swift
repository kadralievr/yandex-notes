//
//  FileNotebook.swift
//  Notes
//
//  Created by Kadraliev Ramil on 28/06/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import UIKit

extension Note {
    
    // Структура для хранения ключей json для избежания дублирования строк и ошибок
    // в методах преобразования Note в json и обратно
    struct JsonKey {
        static let uid = "uid"
        static let title = "title"
        static let content = "content"
        static let color = "color"
        static let importance = "importance"
        static let selfDestructionDate = "selfDestructionDate"
    }
    
    var json: [String: Any] {
        
        var result: [String: Any] = [:]
        
        result[JsonKey.uid] = self.uid // ключ - строка, см структуру JsonKey выше
        result[JsonKey.title] = self.title
        result[JsonKey.content] = self.content
        
        if self.color != UIColor.white {            
            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0
            
            self.color.getRed(&r, green: &g, blue: &b, alpha: &a)
            
            result[JsonKey.color] = [r, g, b, a]
        }
        
        if self.importance != Note.Importance.normal {
            result[JsonKey.importance] = self.importance.rawValue
        }
        
        if let selfDestructionDate = self.selfDestructionDate {
            result[JsonKey.selfDestructionDate] = selfDestructionDate.timeIntervalSince1970
        }
        
        return result
    }
    
    static func parse(json: [String: Any]) -> Note? {
        
        var result: Note?
        
        if let uid = json[JsonKey.uid] as? String,
            let title = json[JsonKey.title] as? String,
            let content = json[JsonKey.content] as? String {
            
            var color: UIColor
            
            if let rgba = json[JsonKey.color] as? [CGFloat] {
                color = UIColor(red: rgba[0], green: rgba[1], blue: rgba[2], alpha: rgba[3])
            } else {
                color = UIColor.white
            }
            
            var importance: Note.Importance
            
            if let importanceRawValue = json[JsonKey.importance] as? Int {
                importance = Note.Importance(rawValue: importanceRawValue) ?? Note.Importance.normal
            } else {
                importance = Note.Importance.normal
            }
            
            var selfDestructionDate: Date?
            
            if let selfDestructionDateTimeInterval = json[JsonKey.selfDestructionDate] as? TimeInterval {
                selfDestructionDate = Date(timeIntervalSince1970: selfDestructionDateTimeInterval)
            } else {
                selfDestructionDate = nil
            }
            
            result = Note(
                uid: uid,
                title: title,
                content: content,
                color: color,
                importance: importance,
                selfDestructionDate: selfDestructionDate
            )
            
        } else {
            result = nil // если обязательных полей в json нет, возвращается nil
        }
        
        return result
    }
    
}
