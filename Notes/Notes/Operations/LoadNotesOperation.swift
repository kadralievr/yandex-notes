//
//  LoadNotesOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция загрузки списка заметок
class LoadNotesOperation: AsyncOperation<LoadNotesResult> {
    
    // Очередь выполнения DB операций
    private let dbOperation: LoadNotesDBOperation
    
    // Очередь выполнения Backend операций
    private let backendOperation: LoadNotesBackendOperation
    
    init(
        backendQueue: OperationQueue,
        dbQueue: OperationQueue,
        accessToken: String,
        gistID: String?,
        context: NSManagedObjectContext
        ) {
        
        // инициализация DB операции
        dbOperation = LoadNotesDBOperation(context: context)
        
        // инициализация backend операции
        backendOperation = LoadNotesBackendOperation(accessToken: accessToken, gistID: gistID)
        
        super.init()
        
        // операция будет запущена, как только выполнится и db операция, и backend операция
        // сами db и backend операции могут выполняться параллельно, т.к. не зависят от результатов выполнения друг друга
        addDependency(dbOperation)
        addDependency(backendOperation)
        
        // добавление db и backend операций в соответствующие очереди
        dbQueue.addOperation(dbOperation)        
        backendQueue.addOperation(backendOperation)
    }
    
    override func main() {
        DDLogInfo("LoadNotesOperation started")
        
        // извлечение результатов выполнения db и backend операций
        if let backendOperationResult = backendOperation.result, let dbOperationResult = dbOperation.result {
            switch (backendOperationResult, dbOperationResult) {
                
            // в случае успеха
            case (.success(let backendNotes), _):
                // возвращаем успешный результат
                result = .success(backendNotes)
                
            case (.gistCreated(let gistID), .success(let dbNotes)):
                result = .gistCreated(gistID, dbNotes)
                
            case (.failure, .success(let dbNotes)):
                result = .success(dbNotes)
                
            default:
                result = .failure
            }
        }
        
        DDLogInfo("LoadNotesOperation finished")
    }
    
}
