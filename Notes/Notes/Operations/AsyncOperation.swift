//
//  AsyncOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 23/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation

// Базовый класс асинхронных операций, обобщенный типом возвращаемого результата
class AsyncOperation<TResult>: Operation {
    
    // Результат выполнения операции
    var result: TResult?
    
    // Всегда True для асинхронных операций
    override var isAsynchronous: Bool {
        return true
    }
    
    // Флаг выполнения операции c поддержкой KVO
    private var _isExecuting = false
    override var isExecuting: Bool {
        get {
            return _isExecuting
        }
        set(value) {
            // Оповещение наблюдателей, что флаг будет изменен
            willChangeValue(forKey: "isExecuting")
            
            _isExecuting = value
            
            // Оповещение наблюдателей, что флаг изменен
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    // Флаг - завершена ли операция, c поддержкой KVO
    private var _isFinished = false
    override var isFinished: Bool {
        get {
            return _isFinished
        }
        set(value) {
            // Оповещение наблюдателей, что флаг будет изменен
            willChangeValue(forKey: "isFinished")
            
            _isFinished = value
            
            // Оповещение наблюдателей, что флаг изменен
            didChangeValue(forKey: "isFinished")
        }
    }
    
    // Флаг наличия отмененных операций-зависимостей
    var hasCancelledDependencies: Bool{
        return dependencies.reduce(false) { $0 || $1.isCancelled }
    }
    
    // Запускает асинхронную операцию
    override func start() {
        
        // если операция имеет отмененную зависимость
        // отменяем ее, помечаем как завершенную
        // завершаем выполнение
        guard !hasCancelledDependencies else {
            cancel()
            isFinished = true
            return
        }
        
        // если операция отменена, помечаем как завершенную
        // завершаем выполнение
        guard !isCancelled else {
            isFinished = true
            return
        }
        
        // Устанавливаем флаг выполнения операции
        isExecuting = true
        
        // Запускаем логику операции. Переопределяется в наследниках
        main()
        
        // Сбрасываем флаг выполнения операции
        isExecuting = false
        
        // Устанавлием флаг завершения операции
        isFinished = true
    }
    
    // Запускает логику операции
    override func main() {
        fatalError("Should be overriden")
    }

}
