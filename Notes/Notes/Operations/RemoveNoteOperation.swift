//
//  RemoveNoteOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция удаления заметки
class RemoveNoteOperation: AsyncOperation<RemoveNoteResult> {
        
    // Очередь выполнения DB операций
    private let dbOperation: RemoveNoteDBOperation
    
    // Очередь выполнения Backend операций
    private let backendOperation: SaveNotesBackendOperation
    
    init(
        uid: String,
        notes: [Note],
        context: NSManagedObjectContext,
        backendQueue: OperationQueue,
        dbQueue: OperationQueue,
        accessToken: String,
        gistID: String
        ) {
        
        // инициализация DB операции
        dbOperation = RemoveNoteDBOperation(uid: uid, context: context)
        
        // инициализация backend операции
        backendOperation = SaveNotesBackendOperation(accessToken: accessToken, gistID: gistID, notes: notes)
        
        super.init()
        
        // установка зависимостей
        // сначала выполняется db операция, затем backend операция
        addDependency(dbOperation)
        addDependency(backendOperation)
        
        // добавление db и backend операций в соответствующие очереди
        dbQueue.addOperation(dbOperation)
        backendQueue.addOperation(backendOperation)
    }
    
    override func main() {
        DDLogInfo("RemoveNoteOperation started")
        
        // извлечение результатов выполнения db и backend операций
        if let backendOperationResult = backendOperation.result, let dbOperationResult = dbOperation.result {
            switch (backendOperationResult, dbOperationResult) {
            case (_, .success):
                result = .success
            default:
                result = .failure
            }
        }
        
        DDLogInfo("RemoveNoteOperation finished")
    }
    
}
