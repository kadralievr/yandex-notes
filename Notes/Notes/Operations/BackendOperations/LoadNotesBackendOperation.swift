//
//  LoadNotesBackendOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack

class LoadNotesBackendOperation: BaseBackendOperation<LoadNotesBackendResult> {
    
    override func main() {
        DDLogInfo("LoadNotesBackendOperation started")
        
        if accessToken.count > 0 {
            let dispatchGroup = DispatchGroup()
            dispatchGroup.enter()
            
            if self.gistID == nil {
                createGist(dispatchGroup)
            } else {
                loadGist(dispatchGroup)
            }
            
            dispatchGroup.wait()
        } else {
            result  = .failure(.unreachable)
        }        
        
        DDLogInfo("LoadNotesBackendOperation finished")
    }
    
    private func createGist(_ dispatchGroup: DispatchGroup) {
        
        DDLogInfo("Creating new gist")
        
        if let url = URL(string: "https://api.github.com/gists") {
            
            var request = URLRequest(url: url)
            request.setValue("token \(self.accessToken)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let gistFile = GistFile(content: "[]")
            let gist = Gist(description: self.gistDescription, files: [self.fileName: gistFile])
            
            do {
                // тело запроса - сериализованный в JSON экземпляр Gist
                let data: Data = try JSONEncoder().encode(gist)
                request.httpBody = data
                
                if let json = String(data: data, encoding: String.Encoding.utf8) {
                    DDLogInfo("New gist: \(json)")
                }
                
                let task = URLSession.shared.dataTask(with: request) {
                    (data: Data?, response: URLResponse?, error: Error?) in
                    
                    if error != nil {
                        
                        self.result = .failure(.gistIDNotSet)
                        DDLogError("An error has occurred during empty gist creation")
                        
                    } else if let response = response as? HTTPURLResponse {
                        
                        switch response.statusCode {
                            
                        // если получен успешный код ответа
                        case 200..<300:
                            // если получен ответ с созданным gist и он успешно декодирован
                            if let data = data, let createdGist = try? JSONDecoder().decode(GistID.self, from: data) {
                                self.result = .gistCreated(createdGist.id)
                                DDLogInfo("Empty gist is created. ID: \(createdGist.id)")
                            }
                            
                        // иначе логируем код ошибки
                        default:
                            self.result = .failure(.gistIDNotSet)
                            DDLogError("Creaty empty gist request has failed with status: \(response.statusCode)")
                        }
                    }
                    
                    dispatchGroup.leave()
                }
                
                task.resume()
                
            } catch {
                result = .failure(.gistIDNotSet)
                DDLogError("An error has occurred during empty gist creation")
            }
        }
    }
    
    private func loadGist(_ dispatchGroup: DispatchGroup) {
        
        if let gistID = gistID, let url = URL(string: "https://api.github.com/gists/\(gistID)") {
            var request = URLRequest(url: url)
            request.setValue("token \(accessToken)", forHTTPHeaderField: "Authorization")
            
            let task = URLSession.shared.dataTask(with: request) {
                (data: Data?, response: URLResponse?, error: Error?) in
                
                if error != nil {
                    
                    self.result = .failure(.unreachable)
                    DDLogError("An error has occurred during gist loading")
                    dispatchGroup.leave()
                    
                } else if let response = response as? HTTPURLResponse {
                    
                    switch response.statusCode {
                        
                    // если получен успешный код ответа
                    case 200..<300:
                        // если получен ответ со списком gist и он успешно декодирован
                        if let data = data, let gist = try? JSONDecoder().decode(Gist.self, from: data) {
                            
                            if let gistFile = gist.files[self.fileName], let data = gistFile.content.data(using: String.Encoding.utf8) {
                                
                                var json: [[String: Any]]
                                
                                // десериализация json или генерация ошибки в случае не успеха
                                do {
                                    json = (try JSONSerialization.jsonObject(with: data, options: [])) as! [[String: Any]]
                                    
                                    // преобразование json словаря в список заметок
                                    let notes = json
                                        .map { Note.parse(json: $0) }
                                        .compactMap { $0 }
                                    
                                    self.result = .success(notes)
                                    
                                } catch {
                                    self.result = .failure(.badContent)
                                }
                            }
                        }
                        dispatchGroup.leave()
                        
                    case 404:
                        self.createGist(dispatchGroup)
                        
                    case 500..<600:
                        self.result = .failure(.serverError)
                        dispatchGroup.leave()
                        
                    default:
                        self.result = .failure(.unreachable)
                        DDLogError("Load gists request has failed with status: \(response.statusCode)")
                        dispatchGroup.leave()
                        
                    }
                }
                
            }
            
            task.resume()
        }
    }
}
