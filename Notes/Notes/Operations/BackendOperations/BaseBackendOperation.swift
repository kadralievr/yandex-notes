//
//  BaseBackendOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation

// Базовый класс Backend операций
class BaseBackendOperation<TResult>: AsyncOperation<TResult> {
    
    internal let accessToken: String
    internal let gistID: String?
    internal let fileName = "ios-course-notes-db"
    internal let gistDescription = "YandexCourse Note List. Kadraliev Ramil"
    
    init(
        accessToken: String,
        gistID: String?
        ) {
        self.accessToken = accessToken
        self.gistID = gistID
        super.init()
    }
}
