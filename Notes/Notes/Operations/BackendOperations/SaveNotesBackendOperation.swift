//
//  SaveNotesBackendOperation.swift
//  Notes
//\

//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack

class SaveNotesBackendOperation: BaseBackendOperation<SaveNotesBackendResult> {
    
    private let notes: [Note]
    
    init(
        accessToken: String,
        gistID: String,
        notes: [Note]
        ) {
        self.notes = notes;
        super.init(accessToken: accessToken, gistID: gistID)
    }
    
    override func main() {
        DDLogInfo("SaveNotesBackendOperation started")
        
        if accessToken.count > 0 {
            do {
                let json = self.notes.map { $0.json }
                let data = try JSONSerialization.data(withJSONObject: json, options: [])
                
                let group = DispatchGroup()
                group.enter()
                
                if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
                    
                    DDLogInfo("Saving JSON: \(jsonString)")
                    
                    let gistFile = GistFile(content: jsonString)
                    let gist = Gist(description: self.gistDescription, files: [self.fileName: gistFile])
                    
                    if let url = URL(string: "https://api.github.com/gists/\(self.gistID!)") {
                        
                        var request = URLRequest(url: url)
                        
                        // установка Authorization заголовка с токеном доступа
                        request.setValue("token \(accessToken)", forHTTPHeaderField: "Authorization")
                        
                        // метод POST
                        request.httpMethod = "PATCH"
                        
                        // тело запроса - сериализованный в JSON экземпляр Gist
                        request.httpBody = try JSONEncoder().encode(gist)
                        
                        let task = URLSession.shared.dataTask(with: request) {
                            (data: Data?, response: URLResponse?, error: Error?) in
                            
                            if error != nil {
                                self.result = .failure(.unreachable)
                                DDLogError("An error has occurred during empty gist creation")
                                
                            } else if let response = response as? HTTPURLResponse {
                                
                                switch response.statusCode {
                                    
                                // если получен успешный код ответа
                                case 200..<300:
                                    self.result = .success
                                    DDLogInfo("Save request completed successfuly")
                                    
                                case 500..<600:
                                    self.result = .failure(.serverError)
                                    
                                // иначе логируем код ошибки
                                default:
                                    self.result = .failure(.gistIDNotSet)
                                    DDLogError("Save gists request has failed with status: \(response.statusCode)")
                                }
                            }
                            
                            group.leave()
                        }
                        
                        task.resume()
                        
                        group.wait()
                    }
                    
                    
                    DDLogInfo("SaveNotesBackendOperation finished")
                }
            } catch {
            }
        } else {
            result = .failure(.unreachable)
        }
    }
    
}
