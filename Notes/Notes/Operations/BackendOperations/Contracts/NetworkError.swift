//
//  NetworkError.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation

// Сетевая ошибка
enum NetworkError {
    case unreachable
    case serverError
    case badContent
    case gistIDNotSet
}
