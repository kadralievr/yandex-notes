//
//  GistID.swift
//  Notes
//
//  Created by Kadraliev Ramil on 11/08/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation

struct GistID: Codable {
    let id: String
}
