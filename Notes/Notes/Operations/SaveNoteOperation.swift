//
//  SaveNoteOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция сохранения заметки
class SaveNoteOperation: AsyncOperation<SaveNoteResult> {
    
    // Очередь выполнения DB операций
    private let dbOperation: SaveNoteDBOperation
    
    // Очередь выполнения Backend операций
    private let backendOperation: SaveNotesBackendOperation
    
    init(
        note: Note,
        notes: [Note],
        context: NSManagedObjectContext,
        backendQueue: OperationQueue,
        dbQueue: OperationQueue,
        accessToken: String,
        gistID: String
        ) {
        
        // инициализация DB операции
        dbOperation = SaveNoteDBOperation(note: note, context: context)
        
        // инициализация backend операции
        backendOperation = SaveNotesBackendOperation(accessToken: accessToken, gistID: gistID, notes: notes)
        
        super.init()
        
        // операция будет запущена, как только выполнится и db операция, и backend операция
        // сами db и backend операции могут выполняться параллельно, т.к. не зависят от результатов выполнения друг друга
        addDependency(dbOperation)
        addDependency(backendOperation)
        
        // добавление db и backend операций в соответствующие очереди
        dbQueue.addOperation(dbOperation)
        backendQueue.addOperation(backendOperation)
    }
    
    override func main() {
        DDLogInfo("SaveNoteOperation started")
        
        // извлечение результатов выполнения db и backend операций
        if let backendOperationResult = backendOperation.result, let dbOperationResult = dbOperation.result {
            switch (backendOperationResult, dbOperationResult) {
            case (_, .success):
                result = .success
            default:
                result = .failure
            }
        }
        
        DDLogInfo("SaveNoteOperation finished")
    }
    
}
