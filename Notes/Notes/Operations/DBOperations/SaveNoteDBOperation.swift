//
//  SaveNoteDBOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция сохранения заметки в БД. Возвращает значение типа SaveNoteDBResult
class SaveNoteDBOperation: BaseDBOperation<SaveNoteDBResult> {
    
    private let note: Note
    
    init(note: Note, context: NSManagedObjectContext) {
        self.note = note
        super.init(context: context)
    }
    
    override func main() {
        DDLogInfo("SaveNoteDBOperation started")
        
        // поиск уже существующей заметки по uid
        let request = NSFetchRequest<NoteModel>.init(entityName: "NoteModel")
        request.predicate = NSPredicate(format: "uid = %@", note.uid)
        
        do {
            let fetchResult = try context.fetch(request)
            
            // создаем новую запись или обновляем существующую, найденную по uid
            let model = fetchResult.count > 0
                ? fetchResult[0]
                : NoteModel(context: context)
            
            model.uid = note.uid
            model.title = note.title
            model.content = note.content
            model.importance = Int32(note.importance.rawValue)
            model.selfDestructionDate = note.selfDestructionDate
            
            var r: CGFloat = 0
            var g: CGFloat = 0
            var b: CGFloat = 0
            var a: CGFloat = 0
            
            note.color.getRed(&r, green: &g, blue: &b, alpha: &a)
            
            model.colorR = Float(r)
            model.colorG = Float(g)
            model.colorB = Float(b)
            model.colorA = Float(a)
            
            // сохраняем изменения в фоновом контексте
            context.performAndWait {
                do {
                    try context.save()
                } catch {
                    DDLogError("An error has occurred during new note saving: \(error)")
                }
            }
        } catch {
            DDLogError("An error has occurred during new note saving perform: \(error)")
        }
        
        DDLogInfo("SaveNoteDBOperation finished")
    }
    
}
