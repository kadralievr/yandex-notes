//
//  LoadNotesDBOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция загрузки списка заметок из БД
class LoadNotesDBOperation: BaseDBOperation<LoadNotesDBResult> {
    
    override func main() {
        DDLogInfo("LoadNotesDBOperation started")
        
        do {
            // инициализация запроса к БД
            let request = NSFetchRequest<NoteModel>.init(entityName: "NoteModel")
            let requestResult = try context.fetch(request)
            
            var notes = [Note]()
            
            // инициализация объекта Note по записи БД
            for model in requestResult {
                if let uid = model.uid,
                    let title = model.title,
                    let content = model.content {
                    
                    let color = UIColor(red: CGFloat(model.colorR), green: CGFloat(model.colorG), blue: CGFloat(model.colorB), alpha: CGFloat(model.colorA))
                    
                    let note = Note(uid: uid, title: title, content: content, color: color, importance: Note.Importance(rawValue: Int(model.importance)) ?? .normal, selfDestructionDate: model.selfDestructionDate)
                    
                    notes.append(note)
                }
            }
            
            result = .success(notes)
            
        } catch {
            result = .failure
        }
        
        DDLogInfo("LoadNotesDBOperation finished")
    }
    
}
