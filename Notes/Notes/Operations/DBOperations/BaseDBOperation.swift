//
//  BaseDBOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CoreData

// Базовый класс DB операций
class BaseDBOperation<TResult>: AsyncOperation<TResult> {
    
    internal let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
        super.init()
    }
    
}
