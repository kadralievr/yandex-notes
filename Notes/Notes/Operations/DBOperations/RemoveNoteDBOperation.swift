//
//  RemoveNoteDBOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation
import CocoaLumberjack
import CoreData

// Операция удаления заметки из БД
class RemoveNoteDBOperation: BaseDBOperation<RemoveNoteDBResult> {
    
    // Идентификатор удаляемой заметки
    private let uid: String
    
    init(uid: String, context: NSManagedObjectContext) {
        self.uid = uid
        super.init(context: context)
    }
    
    override func main() {
        DDLogInfo("RemoveNoteDBOperation started")
        
        // поиск записи по uid
        let request = NSFetchRequest<NoteModel>.init(entityName: "NoteModel")
        request.predicate = NSPredicate(format: "uid = %@", uid)
        
        do {
            let fetchResult = try context.fetch(request)
            
            // если запись найдена, удаляем
            if fetchResult.count > 0 {
                
                let model = fetchResult[0]
                context.delete(model)
                
                // сохраняем изменения в фоновом контексте
                context.performAndWait {
                    do {
                        try context.save()
                    } catch {
                        DDLogError("An error has occurred during note deleting: \(error)")
                    }
                }
            }
        } catch {
            DDLogError("An error has occurred during new note deleting perform: \(error)")
        }        
        
        DDLogInfo("RemoveNoteDBOperation finished")
    }
    
}
