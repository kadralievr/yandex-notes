//
//  RemoveNoteDBOperation.swift
//  Notes
//
//  Created by Kadraliev Ramil on 27/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import Foundation

// Результат выполнения RemoveNoteDBOperation
enum RemoveNoteDBResult {
    case success
    case failure
}
