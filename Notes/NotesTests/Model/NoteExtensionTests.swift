//
//  NoteExtensionTests.swift
//  NotesTests
//
//  Created by Kadraliev Ramil on 05/07/2019.
//  Copyright © 2019 Ramil Kadraliev. All rights reserved.
//

import XCTest
@testable import Notes

class NoteExtensionTests: XCTestCase {

    func testJsonShouldReturnJsonDictionary() {
        let note = Note(title: "title", content: "content", importance: Note.Importance.important)
        let json = note.json
        
        XCTAssertEqual(note.title, json["title"] as? String)
        XCTAssertEqual(note.content, json["content"] as? String)
        XCTAssertEqual(note.importance.rawValue, json["importance"] as? Int)
        XCTAssertNil(json["color"])
        XCTAssertNil(json["selfDestructionDate"])
    }
    
    func testJsonShouldReturnJsonDictionaryWithColorIfNotWhite() {
        let note = Note(title: "title", content: "content", color: UIColor.green, importance: Note.Importance.important)
        let json = note.json
        
        XCTAssertEqual("00FF00FF", json["color"] as? String)
    }
    
    func testJsonShouldReturnJsonDictionaryWithSelfDestructionDateIfSet() {
        let date = Date()
        let timestamp = date.timeIntervalSince1970
        let note = Note(title: "title", content: "content", importance: Note.Importance.important, selfDestructionDate: date)
        let json = note.json
        
        XCTAssertEqual(timestamp, json["selfDestructionDate"] as? TimeInterval)
    }
    
    func testParseShouldReturnNoteByJsonDictionary() {
        let uid = "uid"
        let title = "title"
        let content = "content"
        let json: [String: Any] = ["content": content, "uid": uid, "title": title, "importance": 2]
        let note = Note.parse(json: json)
        
        XCTAssertEqual(note?.uid, uid)
        XCTAssertEqual(note?.title, title)
        XCTAssertEqual(note?.content, content)
        XCTAssertEqual(note?.importance, Note.Importance.important)
    }
    
    func testParseShouldReturnNilIfJsonDoesNotHaveRequiredFields() {
        let uid = "uid"
        let title = "title"
        let content = "content"
        
        var json: [String: Any] = ["uid": uid, "title": title]
        var note = Note.parse(json: json)
        
        XCTAssertNil(note)
        
        json = ["content": content, "title": title]
        note = Note.parse(json: json)
        
        XCTAssertNil(note)
        
        json = ["content": content, "uid": uid]
        note = Note.parse(json: json)
        
        XCTAssertNil(note)
    }
    
    func testParseShouldReturnNormalImportanceIfNotSet() {
        let json: [String: Any] = ["uid": "uid", "title": "title", "content": "content"]
        let note = Note.parse(json: json)
        
        XCTAssertEqual(Note.Importance.normal, note?.importance)
    }
    
    func testParseShouldReturnColorIfSet() {
        let json: [String: Any] = ["uid": "uid", "title": "title", "content": "content", "color": "00FF00FF"]
        let note = Note.parse(json: json)
        
        XCTAssertEqual(UIColor.green.hashValue, note?.color.hashValue)
    }
    
    func testParseShouldReturnSelfDestructionDateIfSet() {
        let date = Date()
        let timestamp = date.timeIntervalSince1970
        
        let json: [String: Any] = ["uid": "uid", "title": "title", "content": "content", "selfDestructionDate": timestamp]
        let note = Note.parse(json: json)
        
        XCTAssertEqual(timestamp, note?.selfDestructionDate?.timeIntervalSince1970)
    }
}
